﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace OSocketMC
{
    namespace TCPSocketMC
    {
        class TSocket
        {
            public const int MAX_TCP_BYTESIZE = 5;
            public const byte SOH = 1;
            public const byte SOT = 2;
            //public const byte EOT = 3;
            public const byte EOTR = 4;

            public static byte[] SetHeader(byte[] sBytes, bool setHeaderInfo = false)
            {
                byte[] tmp = new byte[1 + sBytes.Length + 1];
                tmp[0] = (setHeaderInfo) ? SOH : SOT; // // START OF HEADING || START OF TEXT
                tmp[sBytes.Length + 1] = EOTR; //END OF TRANSMISSION
                Array.Copy(sBytes, 0, tmp, 1, sBytes.Length);

                return tmp;
            }
            public static string RemoveHeader(List<byte> rByteList) // 안쓰임
            {
                rByteList.RemoveAt(0);
                rByteList.RemoveAt(rByteList.Count - 1);
                return Encoding.UTF8.GetString(rByteList.ToArray());
            }
        }

        public enum OSocketErrorCode
        {
            ConnectFail,
            RequestConnectFail,
            MissingAddress
        }
        
    }
}
