﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace OSocketMC
{
    class OSocket
    {
        public static async Task<IPEndPoint> GetIPEndPointAsync(string ipAddress, int portNumber)
        {
            IPEndPoint ipEndPoint = null;
            IPAddress ipAddr;
            if (IPAddress.TryParse(ipAddress, out ipAddr))
            {
                return new IPEndPoint(ipAddr, portNumber);
            }
            IPAddress[] AddressList = await Dns.GetHostAddressesAsync(ipAddress);
            foreach (IPAddress i in AddressList)
            {
                if (i.AddressFamily == AddressFamily.InterNetwork) ipEndPoint = new IPEndPoint(i, portNumber);
            }
            return ipEndPoint;
        }
        public static async Task<IPEndPoint> GetIPEndPointAsync(int portNumber)
        {
            IPAddress[] AddressList = await Dns.GetHostAddressesAsync(Dns.GetHostName());
            IPAddress ipAddress = null;
            foreach (IPAddress i in AddressList)
            {
                if (i.AddressFamily == AddressFamily.InterNetwork) ipAddress = i;
            }
            return new IPEndPoint(ipAddress, portNumber);
        }
        public static async Task<IPAddress> GetAddressAsync()
        {
            IPAddress[] AddressList = await Dns.GetHostAddressesAsync(Dns.GetHostName());
            IPAddress ipAddress = null;
            foreach (IPAddress i in AddressList)
            {
                if (i.AddressFamily == AddressFamily.InterNetwork) ipAddress = i;
            }
            return ipAddress;
        }
    }
}
