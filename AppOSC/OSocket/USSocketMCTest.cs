﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace OSocketMC
{
    namespace UDPSocketMC
    {
        class USSocketMCTest
        {
            private Socket sSocket;
            private IPEndPoint bindIP = new IPEndPoint(IPAddress.Any, 0);
            private bool isRunning;
            private JsonSerializerSettings settings = new JsonSerializerSettings();

            // 이벤트 선언
            public event EventHandler<string> OnListen;
            public event EventHandler<UCEndPointInfo> OnReceive;
            public event EventHandler<UCEndPointInfo> OnSend;
            public event EventHandler<string> OnStop;

            public USSocketMCTest()
            {
                sSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                sSocket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.MulticastTimeToLive, 128);

                settings.Converters.Add(new IPAddressConverter());
                settings.Converters.Add(new IPEndPointConverter());
                settings.Formatting = Formatting.Indented;
            }
            public USSocketMCTest(int portNumber) : this()
            {
                bindIP = new IPEndPoint(IPAddress.Any, portNumber);
            }
            public void JoinMulticastGroup(IPAddress multicastIP)
            {
                sSocket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.AddMembership, new MulticastOption(multicastIP, IPAddress.Any));
            }
            public void DetachMulticastGroup(IPAddress multicastIP)
            {
                sSocket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.DropMembership, new MulticastOption(multicastIP, IPAddress.Any));
            }
            
            public void Start()
            {
                sSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                sSocket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.MulticastTimeToLive, 128);

                sSocket.Bind(bindIP);

                byte[] buffer = new byte[USocket.MAX_UDP_BYTESIZE];
                SocketAsyncEventArgs e = new SocketAsyncEventArgs();
                e.Completed += Receive_Completed;
                e.RemoteEndPoint = bindIP;
                e.SetBuffer(buffer, 0, buffer.Length);

                sSocket.ReceiveFromAsync(e);
                
                OnListen?.Invoke(e, bindIP.ToString());
                isRunning = true;
            }

            private void Receive_Completed(object sender, SocketAsyncEventArgs e)
            {
                if (!isRunning) return; // Dispose 하면 메소드 실행됨
                if (e.BytesTransferred != 0) // 서버로 보내지지 않고 자기자신이 받았을때 0 리턴
                {
                    UCEndPointInfo uCEndPointInfo = JsonConvert.DeserializeObject<UCEndPointInfo>(Encoding.UTF8.GetString(e.Buffer, 0, e.BytesTransferred), settings);
                    uCEndPointInfo.remoteIPEndPoint = (IPEndPoint)e.RemoteEndPoint;
                    uCEndPointInfo.localIPEndPoint = (IPEndPoint)sSocket.LocalEndPoint;
                    OnReceive?.Invoke(e, uCEndPointInfo);
                }
                Socket s = (Socket)sender;
                s.ReceiveFromAsync(e);
            }

            public void SendMessage(string msg, IPEndPoint endPoint, string groupId = "", USMCFlag flag = USMCFlag.@default)
            {
                msg = EncodingJson(msg, endPoint, groupId, flag);
                byte[] buffer = Encoding.UTF8.GetBytes(msg);
                if (buffer.Length > USocket.MAX_UDP_BYTESIZE)
                    throw new ArgumentOutOfRangeException("최대 바이트 수는 " + USocket.MAX_UDP_BYTESIZE + ", 현재 바이트 수 : " + buffer.Length);
                SocketAsyncEventArgs e = new SocketAsyncEventArgs();
                e.Completed += Send_Completed;
                e.RemoteEndPoint = endPoint;
                e.SetBuffer(buffer, 0, buffer.Length);
                
                sSocket.SendToAsync(e);
            }

            private string EncodingJson(string msg, IPEndPoint endPoint, string groupId, USMCFlag flag)
            {
                UCEndPointInfo uCEndPointInfo = new UCEndPointInfo
                {
                    remoteIPEndPoint = endPoint,
                    localIPEndPoint = (IPEndPoint)sSocket.LocalEndPoint,
                    msg = msg,
                    groupId = groupId,
                    time = DateTime.Now.ToString(),
                    flag = flag
                };/*
                UClient uClient = new UClient
                {
                    data = msg,
                    dataType = "string",
                    groupId = groupId,
                    time = DateTime.Now.ToString(),
                    connectionType = USMCConnectionType.client,
                    ipEndPoint = new Dictionary<string, IPEndPoint> { { "remoteIPEndPoint", endPoint }, { "localIPEndPoint", (IPEndPoint)sSocket.LocalEndPoint } }
                };*/
                //var a = JsonConvert.SerializeObject(uClient, settings);
                return JsonConvert.SerializeObject(uCEndPointInfo, settings);
            }

            private void Send_Completed(object sender, SocketAsyncEventArgs e)
            {
                OnSend?.Invoke(e, JsonConvert.DeserializeObject<UCEndPointInfo>(Encoding.UTF8.GetString(e.Buffer, 0, e.BytesTransferred), settings));
            }

            public void Stop()
            {
                isRunning = false; // 순서 중요
                sSocket.Dispose(); // 순서 중요 > receive 이벤트 발생
                OnStop?.Invoke(null, null);

            }
        }
    }
}
