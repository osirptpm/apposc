﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace OSocketMC
{
    namespace TCPSocketMC
    {
        public class TSSocketMC
        {
            private Socket sSocket;
            private List<TCSocket> cSocketList = new List<TCSocket>();
            private IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Any, 0);
            private string address;
            private int portNumber;
            private bool IsListening;


            public event EventHandler OnStart;
            public event EventHandler OnConnect;
            public event EventHandler<string> OnReceive;
            public event EventHandler<string> OnSend;
            public event EventHandler<string> OnSendToGroup;
            public event EventHandler OnDisconnect;
            public event EventHandler OnStop;
            public event EventHandler<OSocketErrorCode> OnError;

            public IPEndPoint ServerEndPoint { get => serverEndPoint; set => serverEndPoint = value; }
            public List<TCSocket> CSocketList { get => cSocketList; }

            private TSSocketMC()
            {
                sSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            }
            public TSSocketMC(IPEndPoint ipEndPoint) : this()
            {
                ServerEndPoint = ipEndPoint;
            }
            public TSSocketMC(int portNumber) : this()
            {
                ServerEndPoint = new IPEndPoint(IPAddress.Any, portNumber);
            }
            public TSSocketMC(string address, int portNumber) : this()
            {
                this.address = address; this.portNumber = portNumber;
            }

            public async void Start()
            {
                sSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                ServerEndPoint = await OSocket.GetIPEndPointAsync(address, portNumber);
                sSocket.Bind(ServerEndPoint);
                sSocket.Listen(0);
                IsListening = true;

                SocketAsyncEventArgs e = new SocketAsyncEventArgs();
                e.Completed += Acceept_Completed;

                sSocket.AcceptAsync(e);

                OnStart?.Invoke(this, EventArgs.Empty);
            }

            private void Acceept_Completed(object sender, SocketAsyncEventArgs e)
            {
                if (!IsListening) return;
                TCSocket cS = new TCSocket();
                cS.OnConnect += OnConnect;
                cS.OnReceive += OnReceive;
                cS.OnDisconnect += Disconnect;
                cS.OnDisconnect += OnDisconnect;
                cS.Socket = e.AcceptSocket; // 이벤트 넣고 나서 소켓 설정 > setter 에서 이벤트 실행
                cS.Receive();
                cSocketList.Add(cS);

                e.AcceptSocket = null;
                sSocket.AcceptAsync(e);
            }

            public void SendToGroup(string msg)
            {
                for (int i = cSocketList.Count - 1; i >= 0; i--)
                {
                    Send(cSocketList[i], msg);
                }
                OnSendToGroup?.Invoke(this, msg);
            }
            private void Send(TCSocket cS, string msg)
            {
                byte[] sBuffer = TSocket.SetHeader(Encoding.UTF8.GetBytes(msg));
                SocketAsyncEventArgs e = new SocketAsyncEventArgs();
                e.SetBuffer(sBuffer, 0, sBuffer.Length);
                e.Completed += Send_Completed;
                e.UserToken = msg;
                cS.Socket.SendAsync(e);
            }

            private void Send_Completed(object sender, SocketAsyncEventArgs e)
            {
                OnSend?.Invoke(this, (string)e.UserToken);
            }

            private void Disconnect(object sender, EventArgs e) // 클라이언트 소켓 연결 해제되면 호출됨
            {
                TCSocket cS = (TCSocket)sender;
                cSocketList.Remove(cS);
            }

            public void Stop()
            {
                for (int i = cSocketList.Count - 1; i >= 0; i--)
                {
                    cSocketList[i].Disconnect();
                }
                //cSocketList.ForEach(cS => cS.Disconnect());
                IsListening = false;
                sSocket.Dispose();

                OnStop?.Invoke(this, EventArgs.Empty);
            }
        }
    }
}
