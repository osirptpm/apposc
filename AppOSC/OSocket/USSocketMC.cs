﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace OSocketMC
{
    namespace UDPSocketMC
    {
        class USSocketMC : USocketMC
        {
            private Dictionary<IPEndPoint, UCEndPointInfo> clients = new Dictionary<IPEndPoint, UCEndPointInfo>();
            private Dictionary<string, HashSet<UCEndPointInfo>> clientGroup = new Dictionary<string, HashSet<UCEndPointInfo>>();

            public event EventHandler<Dictionary<string, HashSet<UCEndPointInfo>>.KeyCollection> ChangedGroupIdCount;


            public USSocketMC() : base()
            {
            }

            public USSocketMC(int portNumber) : base(portNumber)
            {
            }

            protected override void Receive_Completed(object sender, SocketAsyncEventArgs e)
            {
                if (!isRunning || e.BytesTransferred == 0) return;
                base.Receive_Completed(sender, e);
                UCEndPointInfo uCEndPointInfo = JsonConvert.DeserializeObject<UCEndPointInfo>(Encoding.UTF8.GetString(e.Buffer, 0, e.BytesTransferred), settings);
                uCEndPointInfo.remoteIPEndPoint = (IPEndPoint)e.RemoteEndPoint;
                uCEndPointInfo.localIPEndPoint = (IPEndPoint)sSocket.LocalEndPoint;

                if (!clients.ContainsKey(uCEndPointInfo.remoteIPEndPoint))
                {
                    clients[uCEndPointInfo.remoteIPEndPoint] = uCEndPointInfo;
                    if (!clientGroup.ContainsKey(uCEndPointInfo.groupId)) clientGroup[uCEndPointInfo.groupId] = new HashSet<UCEndPointInfo>();
                    SendGroupMessage(uCEndPointInfo.remoteIPEndPoint.ToString(), uCEndPointInfo.groupId, USMCFlag.joinGroup);
                    clientGroup[uCEndPointInfo.groupId].Add(uCEndPointInfo);

                    ChangedGroupIdCount?.Invoke(null, clientGroup.Keys);
                }
                else
                {
                    if (clients[uCEndPointInfo.remoteIPEndPoint].groupId != uCEndPointInfo.groupId) // true 면 changeGroup
                        ChangedGroupId(uCEndPointInfo.remoteIPEndPoint, uCEndPointInfo);
                }
            }

            private void ChangedGroupId(IPEndPoint remoteIPEndPoint, UCEndPointInfo newInfo)
            {
                if (clientGroup[clients[remoteIPEndPoint].groupId].Count > 1) // 그룹에 연결이 2개 이상이면
                    clientGroup[clients[remoteIPEndPoint].groupId].RemoveWhere(u => { return u.remoteIPEndPoint == clients[remoteIPEndPoint].remoteIPEndPoint; }); // 그룹에서 제거
                else clientGroup.Remove(clients[remoteIPEndPoint].groupId); // 아니면 그룹을 제거
                SendGroupMessage(remoteIPEndPoint.ToString(), clients[remoteIPEndPoint].groupId, USMCFlag.detachGroup);

                if (!clientGroup.ContainsKey(newInfo.groupId)) clientGroup[newInfo.groupId] = new HashSet<UCEndPointInfo>();
                SendGroupMessage(remoteIPEndPoint.ToString(), newInfo.groupId, USMCFlag.joinGroup);
                clientGroup[newInfo.groupId].Add(newInfo);

                clients[remoteIPEndPoint] = newInfo;
            }

            public void SendMessage(string msg, USMCFlag flag = USMCFlag.@default)
            {
                for (int i = clients.Keys.Count - 1; i >= 0; i--)
                {
                    SendMessage(msg, clients.Keys.ElementAt(i), clients[clients.Keys.ElementAt(i)].groupId, flag);
                }
            }

            public void SendGroupMessage(string msg, string groupId, USMCFlag flag = USMCFlag.@default)
            {
                if (!clientGroup.ContainsKey(groupId)) return;
                for (int i = clientGroup[groupId].Count - 1; i >= 0; i--)
                {
                    SendMessage(msg, clientGroup[groupId].ElementAt(i).remoteIPEndPoint, groupId, flag);
                }
            }

            public override void Stop()
            {
                Stop(String.Empty);
            }
            public void Stop(string msg)
            {
                SendMessage(msg, USMCFlag.shutDown);
                clients.Clear();
                clientGroup.Clear();
                base.Stop();
            }
        }
    }
}
