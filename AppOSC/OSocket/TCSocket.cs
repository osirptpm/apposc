﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace OSocketMC
{
    namespace TCPSocketMC
    {
        public class TCSocket
        {
            private string groupID = String.Empty;
            private Socket socket;
            private IPEndPoint sIP = new IPEndPoint(IPAddress.Any, 0);
            private string address;
            private int portNumber;

            public event EventHandler OnConnect;
            public event EventHandler OnConnecting;
            public event EventHandler<string> OnReceive;
            public event EventHandler<string> OnSend;
            public event EventHandler OnDisconnect;
            public event EventHandler<OSocketErrorCode> OnError;

            byte[] sBuffer;
            byte[] rBuffer = new byte[TSocket.MAX_TCP_BYTESIZE];
            List<byte> rByteList = new List<byte>();

            public string GroupID { get => groupID; set => groupID = value; }
            public IPEndPoint RemoteEndPoint { get => sIP; set => sIP = value; }

            public Socket Socket {
                get => socket;
                set
                {
                    socket = value;
                    OnConnect?.Invoke(this, EventArgs.Empty);
                }
            }

            public byte[] SBuffer { get => sBuffer; set => sBuffer = value; }
            public byte[] RBuffer { get => rBuffer; set => rBuffer = value; }
            public List<byte> RByteList { get => rByteList; set => rByteList = value; }

            public TCSocket()
            {
                socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp); // 소문자인 이뉴는 setter 실행 피하기위함
            }
            public TCSocket(IPEndPoint ipEndPoint) : this()
            {
                sIP = ipEndPoint;
            }
            public TCSocket(int portNumber) : this()
            {
                sIP = new IPEndPoint(IPAddress.Any, portNumber);
            }
            public TCSocket(string address, int portNumber) : this()
            {
                this.address = address; this.portNumber = portNumber;
            }
            
            public async void Connect()
            {
                try { sIP = await OSocket.GetIPEndPointAsync(address, portNumber); } catch { OnError?.Invoke(this, OSocketErrorCode.MissingAddress); return; }
                socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp); // 소문자인 이유는 setter 실행 피하기위함

                SocketAsyncEventArgs e = new SocketAsyncEventArgs();
                e.RemoteEndPoint = sIP;
                e.Completed += Connect_Completed;
                if (!Socket.ConnectAsync(e)) { OnError?.Invoke(this, OSocketErrorCode.RequestConnectFail); return; }
                OnConnecting?.Invoke(this, EventArgs.Empty);
            }

            private void Connect_Completed(object sender, SocketAsyncEventArgs e)
            {
                if (e.ConnectSocket != null)
                {
                    Receive();
                    SendTCSocketInfo();
                }
                else
                {
                    OnError?.Invoke(this, OSocketErrorCode.ConnectFail); return;
                }
            }

            public void Receive()
            {
                SocketAsyncEventArgs e = new SocketAsyncEventArgs();
                e.SetBuffer(RBuffer, 0, RBuffer.Length);
                e.Completed += Receive_Completed;
                Socket.ReceiveAsync(e);
            }

            private void SendTCSocketInfo()
            {
                string headerInfo = GroupID; // 헤더 내용은 json으로 처리할 계획 // 현재는 그룹ID만
                byte[] sBuffer = TSocket.SetHeader(Encoding.UTF8.GetBytes(headerInfo), true);
                SocketAsyncEventArgs e = new SocketAsyncEventArgs();
                e.SetBuffer(sBuffer, 0, sBuffer.Length);
                e.Completed += SendTCSocketInfo_Completed;

                Socket.SendAsync(e);
            }

            private void SendTCSocketInfo_Completed(object sender, SocketAsyncEventArgs e)
            {
                OnConnect?.Invoke(this, EventArgs.Empty); // 정확히는 서버측에서 그룹설정까지 완료되었다고 메세지가 오면 연결완료 > 나중에 처리할 것
            }

            private void Receive_Completed(object sender, SocketAsyncEventArgs e)
            {
                if (e.BytesTransferred > 0)
                {
                    AnalyzeReceiveByte(e.BytesTransferred);
                    Socket.ReceiveAsync(e);
                }
                else
                {
                    Socket.Shutdown(SocketShutdown.Both);
                    OnDisconnect?.Invoke(this, EventArgs.Empty);
                }
            }

            public void AnalyzeReceiveByte(int bytesTransferred)
            {
                for (int i = 0; i < bytesTransferred; i++)
                {
                    rByteList.Add(rBuffer[i]);
                    if (rBuffer[i] == TSocket.EOTR)
                    {
                        bool isHeader = (rByteList[0] == TSocket.SOH);
                        bool isText = (rByteList[0] == TSocket.SOT);
                        string msg = RemoveHeader();
                        if (isHeader)
                        {
                            ReceiveTCSocketInfo(msg);
                        }
                        else if (isText)
                        {
                            OnReceive?.Invoke(this, msg);
                        }
                        else
                        {
                            throw new Exception("헤더 데이터가 손실되었습니다.");
                        }
                        rByteList.Clear();
                    }
                }
            }

            private void ReceiveTCSocketInfo(string msg)
            {
                groupID = msg;
            }

            private string RemoveHeader()
            {
                rByteList.RemoveAt(0);
                rByteList.RemoveAt(rByteList.Count - 1);
                return Encoding.UTF8.GetString(rByteList.ToArray());
            }

            public void Send(string msg)
            {
                byte[] sBuffer = TSocket.SetHeader(Encoding.UTF8.GetBytes(msg));
                SocketAsyncEventArgs e = new SocketAsyncEventArgs();
                e.SetBuffer(sBuffer, 0, sBuffer.Length);
                e.Completed += Send_Completed;
                e.UserToken = msg;

                Socket.SendAsync(e);
            }

            private void Send_Completed(object sender, SocketAsyncEventArgs e)
            {
                OnSend?.Invoke(this, (string)e.UserToken);
            }

            public void Disconnect()
            {
                Socket.Shutdown(SocketShutdown.Send);
            }
        }
    }
}
