﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace OSocketMC
{
    namespace UDPSocketMC
    {
        class UCSocketMCTest
        {
            private Socket sSocket;
            private IPEndPoint bindIP = new IPEndPoint(IPAddress.Any, 0);
            private IPEndPoint remoteIPEndPoint;
            private bool isRunning;

            UClient uClient;
            private JsonSerializerSettings settings = new JsonSerializerSettings();

            public string Token { get; set; } = String.Empty;
            public string GroupId { get; set; } = String.Empty;
            public USMCConnectionType ConnectionType { get; set; } = USMCConnectionType.client;

            // 이벤트 선언
            public event EventHandler<string> OnListen;
            public event EventHandler<UClient> OnReceive;
            public event EventHandler<UClient> OnSend;
            public event EventHandler<string> OnStop;

            private UCSocketMCTest()
            {
                sSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                sSocket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.MulticastTimeToLive, 128);

                settings.Converters.Add(new IPAddressConverter());
                settings.Converters.Add(new IPEndPointConverter());
                settings.Formatting = Formatting.Indented;
            }
            public UCSocketMCTest(IPEndPoint remoteIPEndPoint, int bindPort) : this()
            {
                this.remoteIPEndPoint = remoteIPEndPoint;
                bindIP = new IPEndPoint(IPAddress.Any, bindPort);
            }
            public void JoinMulticastGroup(IPAddress multicastIP)
            {
                sSocket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.AddMembership, new MulticastOption(multicastIP, IPAddress.Any));
            }
            public void DetachMulticastGroup(IPAddress multicastIP)
            {
                sSocket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.DropMembership, new MulticastOption(multicastIP, IPAddress.Any));
            }
            
            public void Connect()
            {
                sSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                sSocket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.MulticastTimeToLive, 128);

                sSocket.Bind(bindIP);

                byte[] buffer = new byte[USocket.MAX_UDP_BYTESIZE];
                SocketAsyncEventArgs e = new SocketAsyncEventArgs();
                e.Completed += Receive_Completed;
                e.RemoteEndPoint = bindIP;
                e.SetBuffer(buffer, 0, buffer.Length);

                sSocket.ReceiveFromAsync(e);

                doHandshake();
                
                OnListen?.Invoke(e, bindIP.ToString());
                isRunning = true;
            }

            private void doHandshake()
            {
                // 처음 정보 패킷 보내기
                uClient = new UClient
                {
                    PacketType = USMCPacketType.infoPacket,
                    Token = Token,
                    GroupId = GroupId,
                    ConnectionType = ConnectionType,
                    ConnectedTime = DateTime.Now.ToString(),
                    Flag = USMCFlag.handshake,
                    IpEndPoint = new USMCIPInfoPacket { RemoteIPEndPoint = remoteIPEndPoint, LocalIPEndPoint = (IPEndPoint)sSocket.LocalEndPoint }
                };
                SendBuffer(CreateSendBuffer(), Send_Completed);
            }

            private void Receive_Completed(object sender, SocketAsyncEventArgs e)
            {
                if (!isRunning) return; // Dispose 하면 메소드 실행됨
                if (e.BytesTransferred != 0) // 서버로 보내지지 않고 자기자신이 받았을때 0 리턴
                {
                    UClient u = JsonConvert.DeserializeObject<UClient>(Encoding.UTF8.GetString(e.Buffer, 0, e.BytesTransferred), settings);
                    if (u.PacketType == USMCPacketType.dataPacket) ///////////// 이 이프문 다시해야함 정리해서 
                    {
                        uClient.RecevieData = u.SendData;
                        uClient.Time = u.Time;
                        //USMCDataPacket a = (USMCDataPacket)u;
                        //uClient = a;
                        /*uClient.ConnectedTime = u.Time;
                        uClient.Token = u.Token;
                        uClient.GroupId = u.GroupId;
                        uClient.IpEndPoint = u.IpEndPoint;
                        uClient.Flag = u.Flag;
                        uClient.*/
                    }
                    else
                    {
                        uClient.Flag = u.Flag;
                        uClient.Token = u.Token;
                    }
                    OnReceive?.Invoke(e, uClient);
                }
                Socket s = (Socket)sender;
                s.ReceiveFromAsync(e);
            }

            private byte[] CreateSendBuffer()
            {
                string json;
                if (uClient.PacketType == USMCPacketType.dataPacket)
                    json = JsonConvert.SerializeObject((USMCDataPacket)uClient, settings); 
                else json = JsonConvert.SerializeObject((USMCInfoPacket)uClient, settings);

                byte[] buffer = Encoding.UTF8.GetBytes(json);
                if (buffer.Length > USocket.MAX_UDP_BYTESIZE)
                    throw new ArgumentOutOfRangeException("최대 바이트 수는 " + USocket.MAX_UDP_BYTESIZE + ", 현재 바이트 수 : " + buffer.Length);
                return buffer;
            }

            private void SendBuffer(byte[] buffer, EventHandler<SocketAsyncEventArgs> send_Completed)
            {
                SocketAsyncEventArgs e = new SocketAsyncEventArgs();
                e.Completed += send_Completed;
                e.RemoteEndPoint = uClient.IpEndPoint.RemoteIPEndPoint;
                e.SetBuffer(buffer, 0, buffer.Length);

                sSocket.SendToAsync(e);
            }

            public void SendMessage(string msg)
            {
                uClient.PacketType = USMCPacketType.dataPacket;
                uClient.SendData.Data = msg;
                uClient.SendData.Type = "string";
                uClient.Time = DateTime.Now.ToString();

                SendBuffer(CreateSendBuffer(), Send_Completed);
            }
            /*
            public void SendMessage(string msg, IPEndPoint endPoint, string groupId = "", USMCFlag flag = USMCFlag.@default)
            {
                msg = EncodingJson(msg, endPoint, groupId, flag);
                byte[] buffer = Encoding.UTF8.GetBytes(msg);
                if (buffer.Length > USocket.MAX_UDP_BYTESIZE)
                    throw new ArgumentOutOfRangeException("최대 바이트 수는 " + USocket.MAX_UDP_BYTESIZE + ", 현재 바이트 수 : " + buffer.Length);
                SocketAsyncEventArgs e = new SocketAsyncEventArgs();
                e.Completed += Send_Completed;
                e.RemoteEndPoint = endPoint;
                e.SetBuffer(buffer, 0, buffer.Length);

                sSocket.SendToAsync(e);
            }*/
            
            private void Send_Completed(object sender, SocketAsyncEventArgs e)
            {
                OnSend?.Invoke(e, JsonConvert.DeserializeObject<UClient>(Encoding.UTF8.GetString(e.Buffer, 0, e.BytesTransferred), settings));
            }

            public void Disconnect()
            {
                isRunning = false; // 순서 중요
                sSocket.Dispose(); // 순서 중요 > receive 이벤트 발생
                OnStop?.Invoke(null, null);

            }
        }
    }
}
