﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace OSocketMC
{
    namespace UDPSocketMC
    {
        class USocket
        {
            public const int MAX_UDP_BYTESIZE = 8192;
        }

        public enum USMCFlag
        {
            handshake,
            joinGroup,
            @default,
            detachGroup,
            shutDown
        }

        public enum USMCConnectionType
        {
            server,
            client,
            admin
        }

        public enum USMCPacketType
        {
            dataPacket,
            infoPacket
        }

        /*
         * 데이터
         * 데이터타입 (스트링, 바이너리)
         * 원격지 주소
         * 로컬 주소
         * 시간
         * 그룹명
         */

        class USMCIPInfoPacket
        {
            public IPEndPoint RemoteIPEndPoint { get; set; }
            public IPEndPoint LocalIPEndPoint { get; set; }
        }

        class USMCData
        {
            public string Data { get; set; } = String.Empty;
            public string Type { get; set; } = String.Empty;
        }

        class UClient
        {
            public USMCPacketType PacketType { get; set; }
            public string Token { get; set; } = String.Empty;
            public string GroupId { get; set; } = String.Empty;
            public USMCConnectionType ConnectionType { get; set; }
            public USMCData RecevieData { get; set; }
            public USMCData SendData { get; set; }
            public string ConnectedTime { get; set; } = String.Empty;
            public string Time { get; set; } = String.Empty;
            public USMCFlag Flag { get; set; }
            public USMCIPInfoPacket IpEndPoint { get; set; }
        }

        class USMCDataPacket
        {
            public USMCPacketType PacketType { get; set; }
            public string Token { get; set; }
            public USMCData RecevieData { get; set; }
            public USMCData SendData { get; set; }
            public string Time { get; set; }

            public static explicit operator USMCDataPacket(UClient v)
            {
                USMCDataPacket data = new USMCDataPacket
                {
                    PacketType = v.PacketType,
                    Token = v.Token,
                    RecevieData = v.RecevieData,
                    SendData = v.SendData,
                    Time = v.Time,
                };
                return data;
            }
        }

        class USMCInfoPacket
        {
            public USMCPacketType PacketType { get; set; }
            public string Token { get; set; }
            public string GroupId { get; set; }
            public USMCConnectionType ConnectionType { get; set; }
            public string ConnectedTime { get; set; }
            public string Time { get; set; }
            public USMCFlag Flag { get; set; }
            public USMCIPInfoPacket IpEndPoint { get; set; }

            public static explicit operator USMCInfoPacket(UClient v)
            {
                USMCInfoPacket info = new USMCInfoPacket
                {
                    PacketType = v.PacketType,
                    Token = v.Token,
                    GroupId = v.GroupId,
                    ConnectionType = v.ConnectionType,
                    ConnectedTime = v.ConnectedTime,
                    Time = v.Time,
                    Flag = v.Flag,
                    IpEndPoint = v.IpEndPoint
                };
                return info;
            }
        }


        public class UCEndPointInfo
        {
            public string msg { get; set; }
            public string groupId { get; set; }
            public string time { get; set; }
            public USMCFlag flag { get; set; }
            public IPEndPoint localIPEndPoint { get; set; }
            public IPEndPoint remoteIPEndPoint { get; set; }
        }

        class IPAddressConverter : JsonConverter
        {
            public override bool CanConvert(Type objectType)
            {
                return (objectType == typeof(IPAddress));
            }

            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                IPAddress ip = (IPAddress)value;
                writer.WriteValue(ip.ToString());
            }

            public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
            {
                JToken token = JToken.Load(reader);
                return IPAddress.Parse(token.Value<string>());
            }
        }
        class IPEndPointConverter : JsonConverter
        {
            public override bool CanConvert(Type objectType)
            {
                return (objectType == typeof(IPEndPoint));
            }

            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                IPEndPoint ep = (IPEndPoint)value;
                writer.WriteStartObject();
                writer.WritePropertyName("Address");
                serializer.Serialize(writer, ep.Address);
                writer.WritePropertyName("Port");
                writer.WriteValue(ep.Port);
                writer.WriteEndObject();
            }

            public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
            {
                JObject jo = JObject.Load(reader);
                IPAddress address = jo["Address"].ToObject<IPAddress>(serializer);
                int port = jo["Port"].Value<int>();
                return new IPEndPoint(address, port);
            }
        }
    }
}
