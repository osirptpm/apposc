﻿using OSocketMC.TCPSocketMC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Input;

namespace AppOSC.Pages
{
    class PageTSSocketMCInterface : PageOSocketInterface
    {
        private TSSocketMC serverSocket;

        public PageTSSocketMCInterface()
        {
            ConnectBtn.Content = "Start";
        }

        private void TSSocketMCAddEvent()
        {
            serverSocket.OnStart += (async (s, e) =>
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    LogTBox.Text += "서버 시작 : " + serverSocket.ServerEndPoint + "\n";
                    LogTBoxSViewer.ChangeView(0, LogTBoxSViewer.ExtentHeight, 1);
                    ConnectBtn.Content = "Stop";
                });
            });

            serverSocket.OnConnect += (async (s, e) => {
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    TCSocket S = (TCSocket)s;
                    LogTBox.Text += "클라이언트 접속 : " + S.Socket.RemoteEndPoint + "\n";
                    LogTBoxSViewer.ChangeView(0, LogTBoxSViewer.ExtentHeight, 1);
                });
            });
            serverSocket.OnReceive += (async (s, e) => {
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    LogTBox.Text += "받기 : " + e + "\n";
                    LogTBoxSViewer.ChangeView(0, LogTBoxSViewer.ExtentHeight, 1);
                });
            });
            serverSocket.OnSendToGroup += (async (s, e) =>
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    LogTBox.Text += "보내기 : " + e + "\n";
                    LogTBoxSViewer.ChangeView(0, LogTBoxSViewer.ExtentHeight, 1);
                });
            });
            serverSocket.OnDisconnect += (async (s, e) =>
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    TCSocket S = (TCSocket)s;
                    LogTBox.Text += "클라이언트 접속 해제 : " + S.Socket.RemoteEndPoint + "\n";
                    LogTBoxSViewer.ChangeView(0, LogTBoxSViewer.ExtentHeight, 1);
                });
            });
            serverSocket.OnStop += (async (s, e) =>
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    LogTBox.Text += "서버 종료\n";
                    LogTBoxSViewer.ChangeView(0, LogTBoxSViewer.ExtentHeight, 1);
                    ConnectBtn.Content = "Start";
                });
            });
            serverSocket.OnError += (async (s, e) =>
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    LogTBox.Text += "에러코드 : " + e + "\n";
                    LogTBoxSViewer.ChangeView(0, LogTBoxSViewer.ExtentHeight, 1);
                });
            });
        }

        // xaml 이벤트 핸들러
        protected override void ConnectBtn_Checked(object sender, RoutedEventArgs e)
        {
            serverSocket.Start();
        }

        protected override void ConnectBtn_Unchecked(object sender, RoutedEventArgs e)
        {
            serverSocket.Stop();
        }

        protected override void MsgBtn_Click(object sender, RoutedEventArgs e)
        {
            serverSocket.SendToGroup(MsgTBox.Text);
            MsgTBox.Text = "";
        }

        protected override void MsgTBox_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter && e.KeyStatus.RepeatCount == 1)
            {
                serverSocket.SendToGroup(MsgTBox.Text);
                MsgTBox.Text = "";
            }
        }

        protected override void PrepareSocket(object sender, RoutedEventArgs e)
        {
            serverSocket = new TSSocketMC(IPTBox.Text, int.Parse(PortTBox.Text));
            TSSocketMCAddEvent();
        }
    }
}
