﻿using OSocketMC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

namespace AppOSC.Pages
{
    public abstract partial class PageOSocketInterface : Page
    {
        //public List<DependencyObject> GetChildren { get; } = new List<DependencyObject>();
        //public Dictionary<string, DependencyObject> GetNamedChildren { get; } = new Dictionary<string, DependencyObject>();


        public PageOSocketInterface()
        {
            InitializeComponent();
            Init();
        }

        private async void Init()
        {
            Task<System.Net.IPAddress> t = OSocket.GetAddressAsync();
            //FindChildren(GetChildren, GetNamedChildren, this);
            PortTBox.Text = "8080";
            IPTBox.Text = (await t).ToString();
            PrepareSocket(null, null);
        }
        /*
        public DependencyObject GetChildByName(string name)
        {
            return GetNamedChildren[name];
        }

        private void FindChildren<T>(List<T> resultList, Dictionary<string, T> resultDic, DependencyObject startNode) where T : DependencyObject
        {
            int count = VisualTreeHelper.GetChildrenCount(startNode);
            for (int i = 0; i < count; i++)
            {
                DependencyObject current = VisualTreeHelper.GetChild(startNode, i);
                if ((current.GetType()).Equals(typeof(T)) || (current.GetType().GetTypeInfo().IsSubclassOf(typeof(T))))
                {
                    T asType = (T)current;
                    string name = (string)asType.GetValue(NameProperty);
                    resultList.Add(asType);
                    if (name != String.Empty) resultDic.Add(name, asType);
                }
                FindChildren<T>(resultList, resultDic, current);
            }
        }*/

        // xaml 이벤트 핸들러
        protected abstract void PrepareSocket(object sender, RoutedEventArgs e);
        protected abstract void ConnectBtn_Checked(object sender, RoutedEventArgs e);
        protected abstract void ConnectBtn_Unchecked(object sender, RoutedEventArgs e);
        protected abstract void MsgBtn_Click(object sender, RoutedEventArgs e);
        protected abstract void MsgTBox_KeyDown(object sender, KeyRoutedEventArgs e);
    }
}
