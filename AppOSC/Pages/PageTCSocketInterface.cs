﻿using OSocketMC.TCPSocketMC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Input;

namespace AppOSC.Pages
{
    partial class PageTCSocketInterface : PageOSocketInterface
    {
        private TCSocket clientSocket;

        private void TCSocketAddEvent()
        {
            clientSocket.OnConnecting += (async (s, e) =>
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    TCSocket S = (TCSocket)s;
                    LogTBox.Text += "서버 접속 요청 중 : " + S.RemoteEndPoint + "\n";
                    ConnectBtn.Content = "Connecting";
                    LogTBoxSViewer.ChangeView(0, LogTBoxSViewer.ExtentHeight, 1);
                });
            });
            clientSocket.OnConnect += (async (s, e) =>
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    TCSocket S = (TCSocket)s;
                    LogTBox.Text += "서버 접속 : " + S.Socket.RemoteEndPoint + "\n";
                    LogTBoxSViewer.ChangeView(0, LogTBoxSViewer.ExtentHeight, 1);
                    ConnectBtn.IsChecked = true;
                    ConnectBtn.Content = "Disconnect";
                });
            });
            clientSocket.OnReceive += (async (s, e) =>
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    LogTBox.Text += "받기 : " + e + "\n";
                    LogTBoxSViewer.ChangeView(0, LogTBoxSViewer.ExtentHeight, 1);
                });
            });
            clientSocket.OnSend += (async (s, e) =>
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    LogTBox.Text += "보내기 : " + e + "\n";
                    LogTBoxSViewer.ChangeView(0, LogTBoxSViewer.ExtentHeight, 1);
                });
            });
            clientSocket.OnDisconnect += (async (s, e) =>
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    TCSocket S = (TCSocket)s;
                    LogTBox.Text += "서버 접속 해제 : " + S.Socket.RemoteEndPoint + "\n";
                    LogTBoxSViewer.ChangeView(0, LogTBoxSViewer.ExtentHeight, 1);
                    ConnectBtn.IsChecked = false;
                });
            });
            clientSocket.OnError += (async (s, e) =>
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    TCSocket S = (TCSocket)s;
                    if (e == OSocketErrorCode.ConnectFail)
                    {
                        LogTBox.Text += "서버 접속 실패 : " + S.RemoteEndPoint + "\n";
                        if ((bool)ConnectBtn.IsChecked) clientSocket.Connect();
                    }
                    LogTBoxSViewer.ChangeView(0, LogTBoxSViewer.ExtentHeight, 1);
                });
            });
        }

        // xaml 이벤트 핸들러
        protected override void PrepareSocket(object sender, RoutedEventArgs e)
        {
            clientSocket = new TCSocket(IPTBox.Text, int.Parse(PortTBox.Text));
            TCSocketAddEvent();
            clientSocket.GroupID = "에디엇";
        }

        protected override void ConnectBtn_Checked(object sender, RoutedEventArgs e)
        {
            if (!clientSocket.Socket.Connected)
            {
                clientSocket.Connect();
            }
        }

        protected override void ConnectBtn_Unchecked(object sender, RoutedEventArgs e)
        {
            if (clientSocket.Socket.Connected) clientSocket.Disconnect();
            ConnectBtn.Content = "Connect";
        }

        protected override void MsgBtn_Click(object sender, RoutedEventArgs e)
        {
            clientSocket.Send(MsgTBox.Text);
            MsgTBox.Text = "";
        }

        protected override void MsgTBox_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter && e.KeyStatus.RepeatCount == 1)
            {
                clientSocket.Send(MsgTBox.Text);
                MsgTBox.Text = "";
            }
        }
    }
}
