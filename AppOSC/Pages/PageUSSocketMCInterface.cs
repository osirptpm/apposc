﻿using OSocketMC.UDPSocketMC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Input;

namespace AppOSC.Pages
{
    class PageUSSocketMCInterface : PageOSocketInterface
    {
        private USSocketMC serverSocket;

        private void SocketAddEvent()
        {
            serverSocket.OnListen += async (s, e) =>
            {
                SocketAsyncEventArgs S = (SocketAsyncEventArgs)s;
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    LogTBox.Text += "서버 시작 : [" + S.RemoteEndPoint + "]\n";
                    LogTBoxSViewer.ChangeView(0, LogTBoxSViewer.ExtentHeight, 1);
                });
            };
            serverSocket.OnReceive += async (s, e) =>
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    LogTBox.Text += "클라이언트 [" + e.remoteIPEndPoint + "]로부터 전송된 데이터 : \n" +
                    "원격지 주소 : " + e.remoteIPEndPoint + "\n" +
                    "로컬 주소 : " + e.localIPEndPoint + "\n" +
                    "메세지 : " + e.msg + "\n" +
                    "그룹명 : " + e.groupId + "\n" +
                    "시각 : " + e.time + "\n" +
                    "플래그 : " + e.flag + "\n";
                    LogTBoxSViewer.ChangeView(0, LogTBoxSViewer.ExtentHeight, 1);
                });
            };
            serverSocket.OnSend += async (s, e) =>
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    LogTBox.Text += "클라이언트 [" + e.remoteIPEndPoint.ToString() + "]로 보낸 데이터 : \n" +
                    "원격지 주소 : " + e.remoteIPEndPoint + "\n" +
                    "로컬 주소 : " + e.localIPEndPoint + "\n" +
                    "메세지 : " + e.msg + "\n" +
                    "그룹명 : " + e.groupId + "\n" +
                    "시각 : " + e.time + "\n" +
                    "플래그 : " + e.flag + "\n";
                    LogTBoxSViewer.ChangeView(0, LogTBoxSViewer.ExtentHeight, 1);
                });
            };
            serverSocket.OnStop += async (s, e) =>
            {
                SocketAsyncEventArgs S = (SocketAsyncEventArgs)s;
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    LogTBox.Text += "서버 종료\n";
                    LogTBoxSViewer.ChangeView(0, LogTBoxSViewer.ExtentHeight, 1);
                });
            };
        }

        // xaml 이벤트 핸들러
        protected override void ConnectBtn_Checked(object sender, RoutedEventArgs e)
        {
            serverSocket.Run();
        }

        protected override void ConnectBtn_Unchecked(object sender, RoutedEventArgs e)
        {
            serverSocket.Stop();
        }

        protected override void MsgBtn_Click(object sender, RoutedEventArgs e)
        {
            if (GroupIDTBox.Text == "") serverSocket.SendMessage(MsgTBox.Text);
            else serverSocket.SendGroupMessage(MsgTBox.Text, GroupIDTBox.Text);
            MsgTBox.Text = "";
        }

        protected override void MsgTBox_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter && e.KeyStatus.RepeatCount == 1)
            {
                MsgBtn_Click(null, null);
            }
        }

        protected override void PrepareSocket(object sender, RoutedEventArgs e)
        {
            GroupIDTBox.Text = "";
            serverSocket = new USSocketMC(int.Parse(PortTBox.Text));
            SocketAddEvent();
        }
    }
}
