﻿using OSocketMC;
using OSocketMC.UDPSocketMC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Input;

namespace AppOSC.Pages
{
    class PageUCSocketMCInterface : PageOSocketInterface
    {
        private USocketMC clientSocket;
        private IPEndPoint IPEndPoint;
        private int portNumber;

        public PageUCSocketMCInterface(int portNumber)
        {
            this.portNumber = portNumber;
        }

        private void SocketAddEvent()
        {
            clientSocket.OnReceive += async (s, e) =>
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    string log;
                    switch (e.flag)
                    {
                        case USMCFlag.joinGroup: log = "[" + e.msg + "]님이 그룹 " + e.groupId + "에 참여하셨습니다.\n"; break;
                        case USMCFlag.detachGroup: log = "[" + e.msg + "]님이 그룹 " + e.groupId + "에서 나가셨습니다.\n"; break;
                        case USMCFlag.shutDown: log = "서버 [" + e.remoteIPEndPoint + "]가 종료되었습니다.\n"; break;
                        default: log = "서버 [" + e.remoteIPEndPoint + "]로부터 전송된 메세지 : \n" + e.msg + "\n"; break;
                    }
                    LogTBox.Text += log;
                    LogTBoxSViewer.ChangeView(0, LogTBoxSViewer.ExtentHeight, 1);
                });
            };
            clientSocket.OnSend += async (s, e) =>
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    LogTBox.Text += "서버 [" + e.remoteIPEndPoint.ToString() + "]로 보낸 메시지 : " + e.msg + "\n";
                    LogTBoxSViewer.ChangeView(0, LogTBoxSViewer.ExtentHeight, 1);
                });
            };
        }

        private async void GetIPEndPoint()
        {
            IPEndPoint = await OSocket.GetIPEndPointAsync(IPTBox.Text, int.Parse(PortTBox.Text));
        }

        // xaml 이벤트 핸들러
        protected override void ConnectBtn_Checked(object sender, RoutedEventArgs e)
        { 
        }

        protected override void ConnectBtn_Unchecked(object sender, RoutedEventArgs e)
        {
        }

        protected override async void MsgBtn_Click(object sender, RoutedEventArgs e)
        {
            IPEndPoint = await OSocket.GetIPEndPointAsync(IPTBox.Text, int.Parse(PortTBox.Text));
            clientSocket.SendMessage(MsgTBox.Text, IPEndPoint, GroupIDTBox.Text);
            MsgTBox.Text = "";
        }

        protected override void MsgTBox_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter && e.KeyStatus.RepeatCount == 1)
            {
                MsgBtn_Click(null, null);
            }
        }

        protected override void PrepareSocket(object sender, RoutedEventArgs e)
        {
            if (clientSocket != null) clientSocket.Stop();
            clientSocket = new USocketMC(portNumber);
            clientSocket.Run();
            SocketAddEvent();
        }
    }
}
