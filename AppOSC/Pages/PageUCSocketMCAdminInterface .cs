﻿using OSocketMC;
using OSocketMC.UDPSocketMC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Input;

namespace AppOSC.Pages
{
    class PageUCSocketMCAdminInterface : PageOSocketInterface
    {
        private UCSocketMCTest clientSocket;
        private IPEndPoint IPEndPoint;
        private int portNumber;

        public PageUCSocketMCAdminInterface(int portNumber)
        {
            this.portNumber = portNumber;
        }

        private void SocketAddEvent()
        {
            clientSocket.OnReceive += async (s, e) =>
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    string str;
                    if (e.PacketType == USMCPacketType.infoPacket)
                    {
                        USMCInfoPacket ip = (USMCInfoPacket)e;
                        str = "서버로부터 전송된 데이터 : \n" +
                        "토큰 : " + ip.Token + "\n" +
                        "그룹명 : " + ip.GroupId + "\n" +
                        "연결타입 : " + ip.ConnectionType + "\n" +
                        "플래그 : " + ip.Flag + "\n" +
                        "연결수립 시각 : " + ip.ConnectedTime + "\n" +
                        "시각 : " + ip.Time + "\n" +
                        "원격지 주소 : " + ip.IpEndPoint.RemoteIPEndPoint + "\n" +
                        "로컬 주소 : " + ip.IpEndPoint.LocalIPEndPoint + "\n";
                    }
                    else
                    {
                        USMCDataPacket dp = (USMCDataPacket)e;
                        str = "서버로부터 전송된 데이터 : \n" +
                        "토큰 : " + dp.Token + "\n" +
                        "데이터 : " + dp.RecevieData + "\n" +
                        "데이터 타입 : " + dp.RecevieDataType + "\n" +
                        "시각 : " + dp.Time + "\n";
                    }
                    /*
                    switch (e.flag)
                    {
                        case USMCFlag.joinGroup: log = "[" + e.msg + "]님이 그룹 " + e.groupId + "에 참여하셨습니다.\n"; break;
                        case USMCFlag.detachGroup: log = "[" + e.msg + "]님이 그룹 " + e.groupId + "에서 나가셨습니다.\n"; break;
                        case USMCFlag.shutDown: log = "서버 [" + e.remoteIPEndPoint + "]가 종료되었습니다.\n"; break;
                        default: log = "서버 [" + e.remoteIPEndPoint + "]로부터 전송된 메세지 : \n" + e.msg + "\n"; break;
                    }*/
                    LogTBox.Text += str;
                    LogTBoxSViewer.ChangeView(0, LogTBoxSViewer.ExtentHeight, 1);
                });
            };
            clientSocket.OnSend += async (s, e) =>
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    LogTBox.Text += "서버로 보낸 메시지 : " + e.SendData.ToString() + "\n";
                    LogTBoxSViewer.ChangeView(0, LogTBoxSViewer.ExtentHeight, 1);
                });
            };
        }

        private async void GetIPEndPoint()
        {
            IPEndPoint = await OSocket.GetIPEndPointAsync(IPTBox.Text, int.Parse(PortTBox.Text));
        }

        // xaml 이벤트 핸들러
        protected override async void ConnectBtn_Checked(object sender, RoutedEventArgs e)
        {
            IPEndPoint = await OSocket.GetIPEndPointAsync(IPTBox.Text, int.Parse(PortTBox.Text));
            if (clientSocket != null) clientSocket.Disconnect();
            clientSocket = new UCSocketMCTest(IPEndPoint, portNumber);
            clientSocket.Connect();
            SocketAddEvent();
        }

        protected override void ConnectBtn_Unchecked(object sender, RoutedEventArgs e)
        {
            clientSocket.Disconnect();
        }

        protected override void MsgBtn_Click(object sender, RoutedEventArgs e)
        {
            clientSocket.SendMessage(MsgTBox.Text);
            MsgTBox.Text = "";
        }

        protected override void MsgTBox_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter && e.KeyStatus.RepeatCount == 1)
            {
                MsgBtn_Click(null, null);
            }
        }

        protected override void PrepareSocket(object sender, RoutedEventArgs e)
        {
        }
    }
}
